<?php


namespace Drupal\sharepoint_integration\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;

/**
 * Provides a SharePoint Integration form.
*/
class AdvancedSettings extends FormBase {


  protected $messenger;
  protected $moUtilities;
  protected $request;
  protected $moduleExtensionList;



  /**
   *
   */
  public function __construct() {
    $this->messenger = \Drupal::messenger();
    $this->moUtilities = \Drupal::service('sharepoint_integration.mo_utilities');
    $this->request = \Drupal::request();
    $this->moduleExtensionList = \Drupal::service('extension.list.module');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'sharepoint_integration_advanced_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $base_url = $this->request->getSchemeAndHttpHost().$this->request->getBasePath();
    $form['#attached']['library'][] = 'sharepoint_integration/sharepoint_integration';
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $image_path= $base_url . '/' . $this->moduleExtensionList->getPath('sharepoint_integration') . '/images';
    $ajax_icon = '<img class="use-ajax mo_oauth_pro_icon1" src="' . $image_path . '/pro.png" alt="Premium" data-dialog-options="{&quot;width&quot;:&quot;40%&quot;}" data-dialog-type="modal" href="CustomerSupport"><span class="mo_pro_tooltip">Available in the Premium version</span>';
  // Section 1: Roles/Folders Restriction
  $form['roles_folders_restriction'] = [
    '#type' => 'fieldset',
    '#title' => $this->t('Roles/Folders Restriction &nbsp;' . $ajax_icon),
    'description' => [
        '#markup' => t('Map your Drupal Roles / Groups to Sharepoint site URL of Folders to restrict content access '),
      ]
  ];

  $roles = Role::loadMultiple();
  foreach ($roles as $role_id => $role) {
    if ($role_id != 'anonymous') {
      $form['roles_folders_restriction'][$role_id] = [
        '#type' => 'textfield',
        '#title' => $role->label(),
        '#attributes' => ['placeholder' => $this->t('Enter SharePoint Server Relative URL of Folders')],
        '#disabled' => TRUE,
      ];
    }
  }

  $form['roles_folders_restriction']['save'] = [
    '#type' => 'submit',
    '#value' => $this->t('Save'),
    '#attributes' => ['class' => ['premium-feature-button']],
    '#disabled' => TRUE,
  ];

  // Section 2: Sync News And Articles
  $form['sync_news_articles'] = [
    '#type' => 'fieldset',
    '#title' => $this->t('Sync News And Articles &nbsp;' . $ajax_icon),
    'description' => [
        '#markup' => t('Sync All your SharePoint online news and articles into the Drupal pages/articles'),
      ],
  ];

  $form['sync_news_articles']['enable_social_news'] = [
    '#type' => 'checkbox',
    '#title' => $this->t('Enable to Sync SharePoint Social News'),
    '#disabled' => TRUE,
  ];

  $form['sync_news_articles']['enable_social_articles'] = [
    '#type' => 'checkbox',
    '#title' => $this->t('Enable to Sync SharePoint Social Articles'),
    '#disabled' => TRUE,
  ];

  $form['sync_news_articles']['roles_folders_restriction']['save'] = [
    '#type' => 'submit',
    '#value' => $this->t('Save'),
    '#attributes' => ['class' => ['premium-feature-button']],
    '#disabled' => TRUE,
  ];

  $this->moUtilities->showCustomerSupportIcon($form, $form_state);

  return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void { }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void { }

}