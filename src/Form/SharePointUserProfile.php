<?php

namespace Drupal\sharepoint_integration\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 *
 */
class SharePointUserProfile extends FormBase {

  /**
   * The messenger property.
   *
   * @var object
  */

  protected $request;
  protected $moUtilities;
  protected $moduleExtensionList;

  /**
   *
   */
  public function __construct() {
    $this->request = \Drupal::request();
    $this->moUtilities = \Drupal::service('sharepoint_integration.mo_utilities');
    $this->moduleExtensionList = \Drupal::service('extension.list.module');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'sharepoint_integration_share_point_user_profile';
  }

  /**
   * {@inheritdoc}
  */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $base_url = $this->request->getSchemeAndHttpHost().$this->request->getBasePath();
    $form['#attached']['library'][] = 'sharepoint_integration/sharepoint_integration';
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $image_path= $base_url . '/' . $this->moduleExtensionList->getPath('sharepoint_integration') . '/images';
    $ajax_icon = '<img class="use-ajax mo_oauth_pro_icon1" src="' . $image_path . '/pro.png" alt="Premium" data-dialog-options="{&quot;width&quot;:&quot;40%&quot;}" data-dialog-type="modal" href="CustomerSupport"><span class="mo_pro_tooltip">Available in the Premium version</span>';
    $form['manual_provisioning'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Manual Provisioning' . $ajax_icon),
        'description' => [
            '#markup' => t('It provides the feature to Fetch Attributes and to sync individual users'),
      ]
    ];

    $form['manual_provisioning']['sync_individual_user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sync an individual user'),
      '#description' => $this->t('Note: You can find the User Principle Name of user in the user profile in Users tab in your SharePoint Online.'),
      '#attributes' => ['disabled' => 'disabled','placeholder' => $this->t('Enter user principle name of the user to sync')],
    ];

    $form['manual_provisioning']['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Fetch attributes'),
      '#attributes' => ['class' => ['premium-feature-button']],
      '#disabled' => TRUE,
    ];

    $this->moUtilities->showCustomerSupportIcon($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void { }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void { }

}