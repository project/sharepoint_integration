<?php

namespace Drupal\sharepoint_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sharepoint_integration\API\TokenFetcher;
use Drupal\sharepoint_integration\API\GraphAPIClient;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\sharepoint_integration\Utilities\MOUtilities;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Configure SharePoint Integration settings for this site.
*/
class Connection extends ConfigFormBase {

  /**
   * @var \Drupal\sharepoint_integration\API\TokenFetcher
  */
  protected $tokenFetcher;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
  */
  protected $messenger;

  protected $graphAPIClient;
  protected $moUtilities;


  /**
   * Constructs a SharePointSettingsForm object.
   *
   * @param \Drupal\sharepoint_integration\TokenFetcher $token_fetcher
   *   The token fetcher service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
  */

  public function __construct(TokenFetcher $token_fetcher, MessengerInterface $messenger, GraphAPIClient $graph_api_client, MOUtilities $mo_utilities) {
    $this->tokenFetcher = $token_fetcher;
    $this->messenger = $messenger;
    $this->graphAPIClient = $graph_api_client;
    $this->moUtilities = $mo_utilities;


  }

  /**
   * {@inheritdoc}
  */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sharepoint_integration.token_fetcher'),
      $container->get('messenger'),
      $container->get('sharepoint_integration.graph_api_client'),
      $container->get('sharepoint_integration.mo_utilities'),

    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'sharepoint_integration_connection';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['sharepoint_integration.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('sharepoint_integration.settings');

    $form['#attached']['library'][] = 'sharepoint_integration/sharepoint_integration';


    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('To begin content synchronization from SharePoint, create an Azure AD App registrations to make a secure communication with SharePoint content.'),
    ];

    $form['application_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Application ID'),
      '#default_value' => $config->get('application_id'),
      '#placeholder' => $this->t('Enter the Application (Client) ID.'),
      '#description' => $this->t('<b>Note:</b> The Application ID can be found in the Active Directory application\'s Overview tab.'),
      '#required' => TRUE,
    ];

    $form['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $config->get('client_secret'),
      '#placeholder' => $this->t('Enter the Client Secret.'),
      '#description' => $this->t('<b>Note:</b> The Client Secret value is present in the Active Directory application\'s Certificates & Secrets tab.'),
      '#required' => TRUE,
    ];

    $form['tenant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tenant ID'),
      '#default_value' => $config->get('tenant_id'),
      '#placeholder' => $this->t('Enter the Directory (Tenant) ID.'),
      '#description' => $this->t('<b>Note:</b> The Tenant ID is present in the Active Directory application\'s Overview tab.'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configs'),
      '#button_type' => 'primary',
    ];

    $form['test_connection'] = [
      '#type' => 'submit',
      '#value' => t('Test connection'),
      '#submit' => ['::testConnection'],

    ];

    $this->moUtilities->showCustomerSupportIcon($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->configFactory->getEditable('sharepoint_integration.settings')
      ->set('application_id', $form_state->getValue('application_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('tenant_id', $form_state->getValue('tenant_id'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  public function testConnection(){
    try {
      $accessToken = $this->tokenFetcher->fetchToken('client_credentials', [
        'scope' => 'https://graph.microsoft.com/.default',
      ]);
      $endpoint = '/v1.0/sites?search=*&$select=id,displayName';
      $sitesData = $this->graphAPIClient->getRequest($endpoint, $accessToken);
      $this->messenger->addStatus($this->t('Successfully connected to the SharePoint sites. Please navigate to the <a href="'.Url::fromRoute('sharepoint_integration.preview_content')->toString().'">Preview Folders / Files</a> tab to preview the fetched details.', ));
      \Drupal::logger('sharepoint_integration')->info(print_r($sitesData, TRUE));
    }
    catch (\Exception $e) {
        $this->messenger->addError($this->t('@message', ['@message' => $e->getMessage()]));
    }
  }
}