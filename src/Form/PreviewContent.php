<?php

namespace Drupal\sharepoint_integration\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\sharepoint_integration\API\TokenFetcher;
use Drupal\sharepoint_integration\API\GraphAPIClient;
use Drupal\sharepoint_integration\Utilities\MOUtilities;
use Drupal\Core\Url;
use Drupal\sharepoint_integration\Utilities\TableGenerator;


class PreviewContent extends FormBase {

  protected $tokenFetcher;
  protected $graphAPIClient;
  protected $tableGenerator;
  protected $moUtilities;


  public function __construct(TokenFetcher $token_fetcher, GraphAPIClient $graph_api_client,TableGenerator $table_generator, MOUtilities $mo_utilities) {
    $this->tokenFetcher = $token_fetcher;
    $this->graphAPIClient = $graph_api_client;
    $this->tableGenerator = $table_generator;
    $this->moUtilities = $mo_utilities;

  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sharepoint_integration.token_fetcher'),
      $container->get('sharepoint_integration.graph_api_client'),
      $container->get('sharepoint_integration.table_generator'),
      $container->get('sharepoint_integration.mo_utilities')

    );
  }

  public function getFormId() {
    return 'sharepoint_integration_preview_content_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['sharepoint_integration.settings'];
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('sharepoint_integration.settings');
    $form['#attached']['library'][] = 'sharepoint_integration/sharepoint_integration';
    $this->moUtilities->showCustomerSupportIcon($form, $form_state);
    if(empty($config->get('client_secret'))){
      $form['description'] = [
        '#type' => 'item',
        '#markup' => $this->t('Please provide the Client ID, Secret, and Tenant ID of the Azure App Registration on the <a href="'.Url::fromRoute('sharepoint_integration.connection')->toString().'">Connection</a> tab.'),
      ];

      return $form;
    }
    try {

      $accessToken = $this->tokenFetcher->fetchToken('client_credentials', ['scope' => 'https://graph.microsoft.com/.default']);
      $sites = $this->graphAPIClient->getRequest('/v1.0/sites?search=*&$select=id,displayName', $accessToken);

      $options = ['- Select -' => '- Select -'];
      foreach ($sites['value'] as $site) {
        $options[$site['id']] = $site['displayName'];
      }

      $form['sharepoint_site'] = [
        '#type' => 'select',
        '#title' => $this->t('Select SharePoint Site'),
        '#default_value' => $config->get('sharepoint_site'),
        '#options' => $options,
        '#attributes' => ['style' => 'width: 50%;'],
        '#ajax' => [
          'callback' => '::loadDirectories',
          'wrapper' => 'directories-wrapper',
        ],
      ];

      $drive_options = $config->get('sharepoint_drive_options') ? json_decode(gzuncompress($config->get('sharepoint_drive_options'))) : [];

      $form['directories'] = [
        '#type' => 'select',
        '#title' => $this->t('Select Directory'),
        '#attributes' => ['style' => 'width: 50%;'],
        '#prefix' => '<div id="directories-wrapper">',
        '#default_value' => $config->get('sharepoint_drive'),
        '#suffix' => '</div>',
        '#options' => $drive_options,
        '#validated' => TRUE,
        '#ajax' => [
          'callback' => '::loadFiles',
          'wrapper' => 'files-wrapper',
        ],
      ];
      $table_markup = $config->get('sharepoint_current_drive_files') ? json_decode(gzuncompress($config->get('sharepoint_current_drive_files'))) : '';
      $form['files'] = [
        '#type' => 'markup',
        '#markup' => '<div id="files-wrapper">' . $table_markup . '</div>',
      ];

      return $form;

    } catch (\Exception $e) {
      \Drupal::logger('sharepoint_integration')->error('Error fetching SharePoint sites: ' . $e->getMessage());

      $form['error']=[
        '#markup' => $this->t('Error fetching SharePoint sites.'),
      ];
      return $form;
    }
  }

  public function loadDirectories(array &$form, FormStateInterface $form_state) {
    try {
      $siteId = $form_state->getValue('sharepoint_site');
      $accessToken = $this->tokenFetcher->fetchToken('client_credentials', ['scope' => 'https://graph.microsoft.com/.default']);
      $directories = $this->graphAPIClient->getRequest("/v1.0/sites/$siteId/drives", $accessToken);
      $options = ['- Select -' => '- Select -'];
      foreach ($directories['value'] as $directory) {
        $options[$directory['id']] = $directory['name'];
      }
      $form['directories']['#options'] = $options;
      $this->configFactory->getEditable('sharepoint_integration.settings')
      ->set('sharepoint_site', $siteId)
      ->set('sharepoint_drive_options', gzcompress(json_encode($options)))
      ->clear('sharepoint_current_drive_files')
      ->save();
      return $form['directories'];
    } catch (\Exception $e) {
      \Drupal::logger('sharepoint_integration')->error('Error fetching directories: ' . $e->getMessage());
      return [
        '#markup' => $this->t('Error fetching directories.'),
      ];
    }
  }

  public function loadFiles(array &$form, FormStateInterface $form_state) {
    $directoryId = $form_state->getValue('directories');
    $table_render_array = $this->tableGenerator->getTable($directoryId);
    $renderer = \Drupal::service('renderer');
    $table_markup = $renderer->render($table_render_array);

    $this->configFactory->getEditable('sharepoint_integration.settings')
    ->set('sharepoint_drive', $directoryId)
    ->set('sharepoint_current_drive_files', gzcompress(json_encode($table_markup)))
    ->save();

    $form['files'] = [
      '#type' => 'markup',
      '#markup' => 'A page with the title <i>MO SharePoint</i> has been created under <i>/admin/content</i> displaying the below. End users can access files using that page URL. <div id="files-wrapper">' . $table_markup . '</div>',
    ];

    return $form['files'];
  }
  public function submitForm(array &$form, FormStateInterface $form_state) { }

}