<?php

namespace Drupal\sharepoint_integration\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\sharepoint_integration\Utilities\MOSupport;
use Drupal\sharepoint_integration\Utilities\MOUtilities;

/**
 * Class for handling customer support queries.
 */
class CustomerSupport extends FormBase {

  /**
   * The messenger property.
   *
   * @var object
   */
  protected $messenger;

  /**
   * Constructs a new MoOAuthCustomerRequest object.
   */
  public function __construct() {
    $this->messenger = \Drupal::messenger();
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'mo_request_customer_support';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#prefix'] = '<div id="modal_support_form">';
    $form['#suffix'] = '</div>';
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    $form['customer_support_email_address'] = [
      '#type' => 'email',
      '#title' => t('Email'),
      '#required' => TRUE,
      '#attributes' => ['placeholder' => t('Enter valid email'), 'style' => 'width:99%;margin-bottom:1%;'],
    ];

    $form['customer_support_method'] = [
      '#type' => 'select',
      '#title' => t('How can we help you?'),
      '#attributes' => ['style' => 'width:99%;height:30px;margin-bottom:1%;'],
      '#options' => [
        'I need Technical Support' => t('I need Technical Support'),
        'I want to Schedule a Setup Call/Demo' => t('I want to Schedule a Setup Call/Demo'),
        'Try premium features' => t('Try premium features'),
        'I have a custom requirement' => t('I have a custom requirement'),
        'My reason is not listed here' => t('My reason is not listed here'),
      ],
    ];

    $timezone = [];

    foreach (MOUtilities::$zones as $key => $value) {
      $timezone[$value] = $key;
    }

    $form['date_and_time'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name = "customer_support_method"]' => ['value' => 'I want to Schedule a Setup Call/Demo'],
        ],
      ],
    ];

    $form['date_and_time']['timezone'] = [
      '#type' => 'select',
      '#title' => t('Select Timezone'),
      '#options' => $timezone,
      '#default_value' => 'Etc/GMT',
    ];

    $form['date_and_time']['meeting_time'] = [
      '#type' => 'datetime',
      '#title' => 'Date and Time',
      '#format' => '',
      '#default_value' => DrupalDateTime::createFromTimestamp(time()),
    ];

    $form['customer_support_query'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => t('Description'),
      '#attributes' => ['placeholder' => t('Describe your query here!'), 'style' => 'width:99%'],
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button--primary',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'submitModalFormAjax'],
        'event' => 'click',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $form;
  }

  /**
   * Submit handler for sending support query.
   *
   * @param array $form
   *   The form elements array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The formstate.
   *
   * @return Drupal\Core\Ajax\AjaxResponse
   *   Returns ajaxresponse object.
   */
  public function submitModalFormAjax(array $form, FormStateInterface $form_state) {
    $form_values = $form_state->getValues();
    $email = $form_values['customer_support_email_address'];
    $response = new AjaxResponse();
    // If there are any form errors, AJAX replace the form.
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#modal_support_form', $form));
    }elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      \Drupal::messenger()->addMessage(t('The email address <b><i>' . $email . '</i></b> is not valid.'), 'error');
      $response->addCommand(new ReplaceCommand('#modal_support_form', $form));
    }else{
      $support_for = $form_values['customer_support_method'];
      $query = $form_values['customer_support_query'];
      $query_type = 'Contact Support';
      if ($support_for == 'I want to Schedule a Setup Call/Demo') {
        $timezone = $form_values['timezone'];
        $mo_date = $form['date_and_time']['meeting_time']['#value']['date'];
        $mo_time = $form['date_and_time']['meeting_time']['#value']['time'];
        $query_type = 'Call Request';
      }
      $timezone = !empty($timezone) ? $timezone : NULL;
      $mo_date  = !empty($mo_date) ? $mo_date : NULL;
      $mo_time = !empty($mo_time) ? $mo_time : NULL;
      $support = new MOSupport($email, '', $query, $query_type, $timezone, $mo_date, $mo_time, $support_for);
      $support_response = $support->sendSupportQuery();
      if ($support_response) {
        \Drupal::messenger()->addStatus(t('Support query successfully sent. We will get back to you shortly.'));
      }else{
        \Drupal::messenger()->addError(t('An error has occurred. Please try again to submit your Query, or you can also reach out to <a href="mailto:drupalsupport@xecurify.com">drupalsupport@xecurify.com</a>.'));
      }
      $response->addCommand(new RedirectCommand(Url::fromRoute('sharepoint_integration.connection')->toString()));
    }
    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

}
