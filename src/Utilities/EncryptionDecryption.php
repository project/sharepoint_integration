<?php

namespace Drupal\sharepoint_integration\Utilities;

class EncryptionDecryption {
    private $key;
    private $method;

    public function __construct() {
        $this->key = openssl_digest('GOCSPXdfgrxdqCxu4ATjSrR1t0qwertyuANnt9STzM8', 'sha256', true);
        $this->method = 'AES-128-CBC';
    }

    public function encryptData($data) {
        $ivSize = openssl_cipher_iv_length($this->method);
        $iv = openssl_random_pseudo_bytes($ivSize);
        $encryptedData = openssl_encrypt($data, $this->method, $this->key, OPENSSL_RAW_DATA, $iv);
        return base64_encode($iv . $encryptedData);
    }

    public function decryptData($data) {
        if (!empty($data)) {
            $decodedData = base64_decode($data);
            $ivSize = openssl_cipher_iv_length($this->method);
            $iv = substr($decodedData, 0, $ivSize);
            $encryptedData = substr($decodedData, $ivSize);
            return openssl_decrypt($encryptedData, $this->method, $this->key, OPENSSL_RAW_DATA, $iv);
        }
        return false;
    }
}