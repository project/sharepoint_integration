<?php

namespace Drupal\sharepoint_integration\Utilities;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\sharepoint_integration\API\TokenFetcher;
use Drupal\sharepoint_integration\API\GraphAPIClient;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;

class TableGenerator {

  protected $tokenFetcher;
  protected $graphAPIClient;

  public function __construct(TokenFetcher $token_fetcher, GraphAPIClient $graph_api_client) {
    $this->tokenFetcher = $token_fetcher;
    $this->graphAPIClient = $graph_api_client;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sharepoint_integration.token_fetcher'),
      $container->get('sharepoint_integration.graph_api_client')
    );
  }

  public function getTable($directoryId) {
    $header = [
        'name' => t('File/Folder Name'),
        'lastModified' => t('Last Modified'),
        'size' => t('Size'),
        'downloadLink' => t('Download Link'),
        'previewLink' => t('Preview in SharePoint'),
    ];

    $accessToken = $this->tokenFetcher->fetchToken('client_credentials', ['scope' => 'https://graph.microsoft.com/.default']);
    $data = $this->graphAPIClient->getRequest("/v1.0/drives/$directoryId/root/children", $accessToken);
    $files = [];
    if(isset($data['value'])){
      foreach ($data['value'] as $item) {
        $file = [
          'id' => $item['id'],
          'name' => $item['name'],
          'createdDateTime' => $item['createdDateTime'],
          'lastModifiedDateTime' => $item['lastModifiedDateTime'],
          'webUrl' => isset($item['webUrl'])?$item['webUrl']:'',
          'size' => isset($item['size'])?$item['size']:'',
          'mimeType' => isset($item['file']['mimeType']) ? $item['file']['mimeType'] : '',
          'downloadUrl' => isset($item['@microsoft.graph.downloadUrl']) ? $item['@microsoft.graph.downloadUrl'] : '',
          'createdBy' => [
            'email' => isset($item['createdBy']['user']['email'])?$item['createdBy']['user']['email']:'',
            'displayName' => isset($item['createdBy']['user']['displayName'])?$item['createdBy']['user']['displayName']:'',
            'id' => isset($item['createdBy']['user']['id'])?$item['createdBy']['user']['id']:''
          ],
          'lastModifiedBy' => [
            'email' => isset($item['lastModifiedBy']['user']['email'])?$item['lastModifiedBy']['user']['email']:'',
            'displayName' => isset($item['lastModifiedBy']['user']['displayName'])?$item['lastModifiedBy']['user']['displayName']:'',
            'id' => isset($item['lastModifiedBy']['user']['id'])?$item['lastModifiedBy']['user']['id']:''
          ],
        ];
        $files[] = $file;
      }
    }

    $rows = [];
    foreach ($files as $file) {
      $rows[] = [
        'name' => $file['name'],
        'lastModified' => $file['lastModifiedDateTime'],
        'size' => $file['size'],
        'downloadLink' => !empty($file['downloadUrl']) ? Markup::create('<a class="button button--small" href="'.Url::fromUri($file['downloadUrl'])->toString().'" >Download</a>') : ['#markup' => t('N/A')],
        'previewLink' => !empty($file['webUrl'])?Markup::create('<a class="button button--small" href="'.Url::fromUri($file['webUrl'])->toString().'" >Preview</a>'):['#markup' => t('N/A')],
      ];
    }
    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No files/folders found.'),
    ];
  }
}