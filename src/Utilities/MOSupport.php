<?php

namespace Drupal\sharepoint_integration\Utilities;

use Drupal\sharepoint_integration\Utilities\MOConstants;

/**
 * Class for handling customer support query.
*/
class MOSupport {

  /**
   * The email of user.
   *
   * @var string
   */
  public $email;

  /**
   * The phone of user.
   *
   * @var string
   */
  public $phone;

  /**
   * The query of user.
   *
   * @var string
   */
  public $query;

  /**
   * The type of query.
   *
   * @var string
   */
  public $queryType;

  /**
   * The timezone of user.
   *
   * @var string
   */
  public $moTimezone;

  /**
   * The date of query.
   *
   * @var string
   */
  public $moDate;

  /**
   * The time of query.
   *
   * @var string
   */
  public $moTime;
  public $supportFor;

  protected $httpClient;
  protected $exceptionLogger;


  /**
   *
   * @param string $email
   *   The email of user.
   * @param string $phone
   *   The phone of user.
   * @param string $query
   *   The query of user.
   * @param string $queryType
   *   The query type of user.
   * @param string $moTimezone
   *   The timzone.
   * @param string $moDate
   *   The date.
   * @param string $moTime
   *   The time.
   */
  public function __construct($email, $phone, $query, $queryType = '', $moTimezone = '', $moDate = '', $moTime = '',$supportFor = '') {
    $this->email = $email;
    $this->phone = $phone;
    $this->query = $query;
    $this->queryType = $queryType;
    $this->moTimezone = $moTimezone;
    $this->moDate = $moDate;
    $this->moTime = $moTime;
    $this->supportFor = $supportFor;
    $this->httpClient = \Drupal::httpClient();;
    $this->exceptionLogger = \Drupal::service('sharepoint_integration.exception_logger');

  }

  /**
   * Send support query.
   *
   * @return bool
   *   Return true if query sent successfully else false.
   */
  /**
   * Send support query.
   *
   * @return bool
   *   Return true if query sent successfully else false.
   */
  public function sendSupportQuery() {
    $modules_info = \Drupal::service('extension.list.module')->getExtensionInfo('sharepoint_integration');
    $modules_version = $modules_info['version'];
    $url = MOConstants::BASE_URL . '/moas/api/notify/send';
    $request_for = $this->queryType == 'Trial Request' ? 'Trial' : ($this->queryType == 'Contact Support' ? 'Support' : 'Setup Meeting/Call');
    $subject = $request_for . ' request for Drupal-' . \DRUPAL::VERSION . ' SharePoint Integration Module | ' . $modules_version . ' | '. phpversion() . ' - ' .$this->email;
    $this->query = $this->queryType == 'Trial Request' ? $this->query : $request_for . ' requested for - ' . $this->query;
    $customerKey = "16555";
    $apikey = "fFd2XcvTGDemZvbw1bcUesNJWEqKbbUq";
    $currentTimeInMillis = $this->getTimestamp();
    $stringToHash = $customerKey . $currentTimeInMillis . $apikey;
    $hashValue = hash("sha512", $stringToHash);
    $content = $this->generateContent($modules_version);

    $fields = [
      'customerKey' => $customerKey,
      'sendEmail' => TRUE,
      'email' => [
        'customerKey' => $customerKey,
        'fromEmail' => $this->email,
        'fromName' => 'miniOrange',
        'toEmail' => MOConstants::SUPPORT_EMAIL,
        'toName' => MOConstants::SUPPORT_EMAIL,
        'subject' => $subject,
        'content' => $content,
      ],
    ];

    $header = [
      'Content-Type' => 'application/json',
      'Customer-Key' => $customerKey,
      'Timestamp' => $currentTimeInMillis,
      'Authorization' => $hashValue,
    ];

    $field_string = json_encode($fields);
    $response = $this->callService($url, $field_string, $header);
    $content = json_decode($response, TRUE);
    if (isset($content['status']) && $content['status'] == 'SUCCESS') {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Generate content based on query type.
   *
   * @param string $modules_version
   *   The module version.
   *
   * @return string
   *   The content.
   */
  private function generateContent($modules_version) {
    $company = $_SERVER['SERVER_NAME'];
    if ($this->queryType == 'Call Request') {
      return '<div>Hello, <br><br>Company :<a href="' . $company . '" target="_blank" >' . $company . '</a><br><br>Phone Number:' . $this->phone . '<br><br>Email:<a href="mailto:' . $this->email . '" target="_blank">' . $this->email . '</a><br><br> Timezone: <b>' . $this->moTimezone . '</b><br><br> Date: <b>' . $this->moDate . '</b>&nbsp;&nbsp; Time: <b>' . $this->moTime . '</b><br><br>Query:[DRUPAL ' . \DRUPAL::VERSION . ' SharePoint Integration | ' . $modules_version . ' | PHP ' . phpversion() . ' ] ' . $this->query . '</div>';
    }
    elseif ($this->queryType == 'Contact Support') {
      return '<div>Hello, <br><br>Company :<a href="' . $company . '" target="_blank" >' . $company . '</a><br><br><strong>Support needed for: </strong>' . $this->supportFor . '<br><br>Email:<a href="mailto:' . $this->email . '" target="_blank">' . $this->email . '</a><br><br>Query:[DRUPAL ' . \DRUPAL::VERSION . ' SharePoint Integration | ' . $modules_version . ' | PHP ' . phpversion() . ' ] ' . $this->query . '</div>';
    }
    else {
      return '<div>Hello, <br><br>Company :<a href="' . $company . '" target="_blank" >' . $company . '</a><br><br>Phone Number:' . $this->phone . '<br><br>Email:<a href="mailto:' . $this->email . '" target="_blank">' . $this->email . '</a><br><br>Trial request for:[DRUPAL ' . \DRUPAL::VERSION . ' SharePoint Integration | ' . $modules_version . ' | PHP ' . phpversion() . ' ] ' . $this->query . '</div>';
    }
  }

  /**
   * Return the timestamp in milliseconds.
   *
   * @return int
   *   The current timestamp.
   */
  public function getTimestamp() {
    $url = MOConstants::BASE_URL.'/moas/rest/mobile/get-timestamp';
    $content = $this->callService($url, [], []);
    if (empty($content)) {
      $currentTimeInMillis = round(microtime(TRUE) * 1000);
      return number_format($currentTimeInMillis, 0, '', '');
    }
    return $content;
  }

  /**
   * Makes API request to respective endpoint.
   *
   * @param string $url
   *   The endpoint where we make API request.
   * @param array|string $fields
   *   The API request body.
   * @param array $header
   *   The API request header.
   *
   * @return mixed
   *   The response.
   */
  public function callService($url, $fields, $header = FALSE) {
    $options = [
      'headers' => $header,
      'verify' => FALSE,
      'body' => is_string($fields) ? $fields : json_encode($fields),
      'allow_redirects' => TRUE,
      'http_errors' => FALSE,
      'decode_content' => TRUE,
    ];
    try {
      $response = $this->httpClient->post($url, $options);
      return $response->getBody()->getContents();
    } catch (RequestException $e) {
        $this->exceptionLogger->handleException($e);
    } catch (\Exception $e) {
        $this->exceptionLogger->handleException($e);
    }
  }
}