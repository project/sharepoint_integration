<?php

/**
 * @file
 * Contains constants class.
*/

namespace Drupal\sharepoint_integration\Utilities;

/**
 * Class for handling constants used throughout the project.
 */
class MOConstants {

  const BASE_URL = 'https://login.xecurify.com';
  const SUPPORT_EMAIL = 'drupalsupport@xecurify.com';
}