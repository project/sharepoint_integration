<?php

namespace Drupal\sharepoint_integration\API;

use GuzzleHttp\Client;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\sharepoint_integration\API\ExceptionLogger;

class GraphAPIClient {

  protected $httpClient;
  protected $configFactory;
  protected $exceptionLogger;

  public function __construct(Client $http_client, ConfigFactoryInterface $config_factory, ExceptionLogger $exception_logger) {
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
    $this->exceptionLogger = $exception_logger;
  }

  /**
   * Makes a GET request to the Microsoft Graph API.
   *
   * @param string $endpoint
   *   The Microsoft Graph API endpoint (e.g., '/v1.0/me').
   * @param string $accessToken
   *   The access token to authenticate the request.
   *
   * @return array
   *   The response from the Microsoft Graph API.
   *
   * @throws \Exception
   *   If the request fails.
   */
  public function getRequest($endpoint, $accessToken) {
    $baseUrl = 'https://graph.microsoft.com';
    $url = $baseUrl . $endpoint;
    $requestOptions = [
      'headers' => [
        'Authorization' => 'Bearer ' . $accessToken,
        'Accept' => 'application/json',
      ],
    ];

    try {
      $response = $this->httpClient->get($url, $requestOptions);
      $data = json_decode($response->getBody(), TRUE);
      // Check for errors in the response
      if (empty($data) || isset($data['error'])) {
        $this->exceptionLogger->Exception($response->getBody());
      }
      return $data;
    }
    catch (RequestException $e) {
       $this->exceptionLogger->handleException($e);
    }
    catch (\Exception $e) {
       $this->exceptionLogger->handleException($e);
    }
  }
}