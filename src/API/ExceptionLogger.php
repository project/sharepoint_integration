<?php

namespace Drupal\sharepoint_integration\API;

use GuzzleHttp\Exception\RequestException;

class ExceptionLogger {

  /**
   * Logs the response data.
   *
   * @param string $message
   *   The message to log.
   * @param array $context
   *   Additional context for the log entry.
   */
  public function logInfo($message) {
    \Drupal::logger('sharepoint_integration')->info($message);
  }

  /**
   * Handles exceptions by logging them and rethrowing.
   *
   * @param \Exception $e
   *   The exception to handle.
   *
   * @throws \Exception
   */
  public function handleException($e) {
    if($e instanceof RequestException){
      $response = $e->getResponse();
      $response_body = $response ? $response->getBody()->getContents() : $e->getMessage();
      \Drupal::logger('sharepoint_integration')->error($response_body);
      throw new \Exception($response_body);
    }else{
      \Drupal::logger('sharepoint_integration')->error($e->getMessage());
      throw new \Exception($e->getMessage());
    }
  }

  public function Exception($exp) {
    Drupal::logger('sharepoint_integration')->error($exp);
    throw new \Exception($exp);
  }

}