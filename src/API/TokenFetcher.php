<?php

namespace Drupal\sharepoint_integration\API;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\sharepoint_integration\API\ExceptionLogger;

class TokenFetcher {

  protected $httpClient;
  protected $configFactory;
  protected $exceptionLogger;

  public function __construct(Client $http_client, ConfigFactoryInterface $config_factory, ExceptionLogger $exception_logger) {
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
    $this->exceptionLogger = $exception_logger;

  }
  /**
   * Fetch an access token from the IDP.
   *
   * @param string $grantType
   *   The grant type (e.g., 'authorization_code', 'client_credentials', 'password', 'refresh_token').
   * @param array $params
   *   The parameters needed for the grant type.
   *
   * @return array
   *   The token response.
   *
   * @throws \Exception
   *   If the token request fails.
   */
  public function fetchToken($grantType, array $params) {
    $config = $this->configFactory->get('sharepoint_integration.settings');
    $tenant_id = $config->get('tenant_id');
    $tokenUrl = "https://login.microsoftonline.com/$tenant_id/oauth2/v2.0/token";
    $clientId = $config->get('application_id');
    $clientSecret = $config->get('client_secret');
    $requestOptions = [
      'form_params' => [
        'grant_type' => $grantType,
        'client_id' => $clientId,
        'client_secret' => $clientSecret,
      ] + $params,
    ];
    try{
        $response = $this->httpClient->post($tokenUrl, $requestOptions);
        $data = json_decode($response->getBody(), TRUE);
        // Check if there is no access token in the response
        if (!isset($data['access_token'])) {
         $this->exceptionLogger->Exception($response->getBody());
        }
        return $data['access_token'];
    }catch (RequestException $e){
        $this->exceptionLogger->handleException($e);
    }catch (\Exception $e){
        $this->exceptionLogger->handleException($e);
    }
  }
}