<?php

namespace Drupal\sharepoint_integration\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\EntityTypeManagerInterface;


/**
 * Class ConfigSubscriber.
 */
class ConfigSubscriber implements EventSubscriberInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The alias storage.
   *
   * @var \Drupal\path_alias\AliasStorageInterface
   * Constructs a ConfigSubscriber object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  protected $aliasStorage;

    public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
        $this->configFactory = $config_factory;
        $this->entityTypeManager = $entity_type_manager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array {
        $events[ConfigEvents::SAVE][] = ['onConfigSave'];
        return $events;
    }

    /**
     *
     * @param \Drupal\Core\Config\ConfigCrudEvent $event
     *   The event to process.
     */
    public function onConfigSave(ConfigCrudEvent $event) {
        $config = $event->getConfig();

        if ($config->getName() === 'sharepoint_integration.settings') {
        // Check if the specific configuration key is set.
        if ($config->get('sharepoint_current_drive_files')) {
            $this->createOrUpdateNodeWithAlias($config);
        }
        }
    }

    /**
     * Creates or updates a node and sets an alias.
     *
     * @param \Drupal\Core\Config\ImmutableConfig $config
     *   The configuration object.
     */
    protected function createOrUpdateNodeWithAlias($config) {
        $table_markup = $config->get('sharepoint_current_drive_files') ? json_decode(gzuncompress($config->get('sharepoint_current_drive_files'))) : '';

        $node_storage = $this->entityTypeManager->getStorage('node');
        $nodes = $node_storage->loadByProperties(['title' => 'MO SharePoint']);

        if (!empty($nodes)) {
        $node = reset($nodes);
        $node->set('body', [
            'value' => '<div id="files-wrapper">' . $table_markup . '</div>',
            'format' => 'full_html',
        ]);
        } else {
        $node = Node::create([
            'type' => 'page',
            'title' => 'MO SharePoint',
            'body' => [
            'value' => '<div id="files-wrapper">' . $table_markup . '</div>',
            'format' => 'full_html',
            ],
            'status' => 1,
        ]);
        }
        $node->save();

    }
}