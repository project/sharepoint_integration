## INTRODUCTION

The SharePoint Integration module provides functionality to synchronize directories and files between Microsoft SharePoint and Drupal websites using the Microsoft Graph API. It also allows synchronizing the articles, events, user details, and other content from SharePoint, OneDrive, as well as Office 365 with the Drupal site. With this module, Drupal users can create and view SharePoint and OneDrive documents directly from the Drupal site based on their roles and permissions.

See: https://www.drupal.org/project/sharepoint_integration for further information.


## REQUIREMENTS

NONE

## Recommended modules

https://www.drupal.org/project/usage/rest_api_authentication

https://www.drupal.org/project/miniorange_2fa

https://www.drupal.org/project/user_provisioning

https://www.drupal.org/project/miniorange_oauth_client

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

Follow the below setup guide:
https://www.drupal.org/docs/extending-drupal/contributed-modules/contributed-module-documentation/how-to-connect-microsoft-sharepoint-with-drupal


## MAINTAINERS
miniOrange Drupal team
